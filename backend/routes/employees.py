from flask import Blueprint, jsonify, request
from sqlalchemy.engine import CursorResult
from sqlalchemy.sql import delete, text

import models
from database import session

routes = Blueprint('', url_prefix='/employees', import_name=__name__)


@routes.route("/", methods=['GET'])
def get_employees():
    stmt = """SELECT e.id, e.fio, e.birthday, e.phone_number, e.personal_number, d.department, r.role, p.project
    FROM employees e
    JOIN departments d on d.id = e.department_id
    JOIN roles r on r.id = e.role_id
    JOIN projects p on p.id = e.project_id"""

    employee_id = request.args.get('id')
    where_stmt = f"WHERE e.id = {employee_id}"
    if employee_id:
        stmt += ' ' + where_stmt

    result: CursorResult = session.execute(text(stmt))
    response = []
    for i in result.fetchall():
        response.append({
            "id": i[0],
            "fio": i[1],
            "birthday": i[2],
            "phone_number": i[3],
            "personal_number": i[4],
            "department": i[5],
            "role": i[6],
            "project": i[7],
        })
    return jsonify(response)


@routes.route("/", methods=['POST'])
def post_employees():
    json_ = request.json
    employee = models.Employees(
        department_id=int(json_['departmentId']),
        project_id=int(json_['projectId']),
        role_id=int(json_['roleId']),
        fio=json_['fio'],
        birthday=json_['birthday'],
        phone_number=int(json_['phoneNumber']),
        personal_number=int(json_['personalNumber']),
    )
    session.add(employee)
    session.commit()
    return {"id": employee.id}, 201


@routes.route("/", methods=['PUT'])
def put_employees():
    return 'employees put'


@routes.route("/", methods=['DELETE'])
def delete_employees():
    employee_id = request.args.get('id')

    stmt = (
        delete(models.Employees).
        where(models.Employees.id == employee_id)
    )

    session.execute(stmt)
    session.commit()
    return {"id": employee_id}
