import flask
from flask import request, jsonify
import crud
from routes.employees import routes

app = flask.Flask(__name__)
app.register_blueprint(routes)

@app.route("/properties", methods=['POST'])
def post_property():
    json_ = request.json
    return {
        "id": crud.post_property(**json_)
    }


@app.route("/properties", methods=['GET'])
def get_property():
    property_id = request.args.get('id')
    if property_id:
        result = crud.get_property(property_id)
        if result:
            # return {
            #     'id': result.id,
            #     'classification_name': result.classification_name,
            #     'property_name': result.property_name,
            #     'property_type': result.property_type,
            #     'choices': result.choices,
            # }
            return {
                # 'id': result.id,
                'classification_name': result.classification_name,
                # 'property_name': result.property_name,
                # 'property_type': result.property_type,
                'description': result.description,
                # 'choices': result.choices,
            }
        return {}
    results = crud.get_properties()
    # return {
    #     'results': [{
    #             'id': result.id,
    #             'classification_name': result.classification_name,
    #             'property_name': result.property_name,
    #             'property_type': result.property_type,
    #             'choices': result.choices,
    #         } for result in results]
    # }
    return {
        'results': [{
                # 'id': result.id,
                'classification_name': result.classification_name,
                # 'property_name': result.property_name,
                # 'property_type': result.property_type,
                # 'choices': result.choices,
                'description': result.description,
            } for result in results]
    }


@app.route("/ping")
def ping_test():
    return "PONG"


if __name__ == '__main__':
    app.run(threaded=True, port=5000, debug=True)
