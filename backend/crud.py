from database import session
import models
from sqlalchemy import select
from typing import Optional


def post_property(
        classification_name,
        description,
        property_name,
        property_type,
        choices,
) -> int:
    """Добавление свойства в БД."""
    property_ = models.DynamicPropertiesDirectory(
        classification_name=classification_name,
        description=description,
        property_name=property_name,
        property_type=property_type,
        choices=choices,
    )
    session.add(property_)
    session.commit()
    return property_.id


def get_property(property_id) -> Optional[models.DynamicPropertiesDirectory]:
    query = select(models.DynamicPropertiesDirectory).where(models.DynamicPropertiesDirectory.id == property_id)
    result = session.execute(query).scalars().one_or_none()
    return result


def get_properties() -> list[models.DynamicPropertiesDirectory]:
    query = select(models.DynamicPropertiesDirectory)
    result = session.execute(query).scalars().all()
    return result
