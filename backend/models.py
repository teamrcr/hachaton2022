from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DATE
from sqlalchemy.orm import relationship
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class Employees(Base):
    __tablename__ = "employees"

    id = Column(Integer, primary_key=True)
    department_id = Column(Integer, ForeignKey("departments.id"))
    project_id = Column(Integer, ForeignKey("projects.id"))
    role_id = Column(Integer, ForeignKey("roles.id"))

    fio = Column(String)
    birthday = Column(DATE)
    phone_number = Column(Integer)
    personal_number = Column(Integer)

    department = relationship('Departments')
    project = relationship('Projects')
    role = relationship('Roles')


# Классификаторы
class Departments(Base):
    __tablename__ = "departments"

    id = Column(Integer, primary_key=True)
    department = Column(String)


class Projects(Base):
    __tablename__ = "projects"

    id = Column(Integer, primary_key=True)
    project = Column(String)


class Roles(Base):
    __tablename__ = "roles"

    id = Column(Integer, primary_key=True)
    role = Column(String)


# Dynamic properties
class DynamicPropertiesDirectory(Base):
    __tablename__ = "dynamic_properties_directory"

    id = Column(Integer, primary_key=True)

    classification_name = Column(String)
    description = Column(String)
    property_name = Column(String)
    property_type = Column(String)
    choices = Column(String)


class PropertyHistory(Base):
    __tablename__ = "property_history"

    id = Column(Integer, primary_key=True)

    user = relationship('Employees')
    user_id = Column(Integer, ForeignKey('employees.id'))

    property = relationship('DynamicPropertiesDirectory')
    property_id = Column(Integer, ForeignKey('dynamic_properties_directory.id'))

    value = Column(String)
    start_date = Column(DATE)
    end_date = Column(DATE)
    actually = Column(Boolean)
    source_change = Column(String, default='Костыль Педальков')
