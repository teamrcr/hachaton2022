import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid, ruRU } from '@mui/x-data-grid';
import Button from "@mui/material/Button";
import RestoreIcon from '@mui/icons-material/Restore';
import EditIcon from '@mui/icons-material/Edit';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';

const columns = [
    { field: 'employee', headerName: 'Сотрудник', width: 250 },
    {
        field: 'phone',
        headerName: 'Телефон',
        width: 150,
    },
    {
        field: 'birthDate',
        headerName: 'Дата рождения',
        width: 130,
    },
    {
        field: 'personalNum',
        headerName: 'Табельный №',
        type: 'number',
        width: 110,
    },
    {
        field: 'department',
        headerName: 'Отдел',
        type: 'string',
        width: 250,
    },
    {
        field: 'project',
        headerName: 'Проект',
        type: 'string',
        width: 200,
    },
    {
        field: 'role',
        headerName: 'Роль',
        type: 'string',
        width: 110,
    },
    {
        field: 'actions',
        headerName: 'Действия',
        width: 200,
        renderCell: (cellValues) => (
            <div>
                <Button startIcon={<RestoreIcon fontSize="small" />}></Button>
                <Button startIcon={<EditIcon fontSize="small" />}></Button>
                <Button sx={{color: "red"}} startIcon={<HighlightOffIcon fontSize="small" />}></Button>
            </div>
            )
    }
];

const rows = [
    { id: 1, employee: 'Иванов Иван Иванович', phone: '+79002130012', birthDate: '12.01.1990', personalNum: '43-0123123', department: 'Отедл велосипедов', project: "Агро7", role: 'Разработчик' },
    { id: 2, employee: 'Люк Скайвокер Йодовович', phone: '+79002130012', birthDate: '22.05.1000', personalNum: '01-4443123', department: 'Отедл космоса', project: "StarWars2", role: 'Мастер' },
    { id: 3, employee: 'Иванов Иван Иванович', phone: '+79002130012', birthDate: '12.01.1990', personalNum: '43-0123123', department: 'Отедл велосипедов', project: "Агро7", role: 'Разработчик' },
    { id: 4, employee: 'Иванов Иван Иванович', phone: '+79002130012', birthDate: '12.01.1990', personalNum: '43-0123123', department: 'Отедл велосипедов', project: "Агро7", role: 'Разработчик' },

];

export const ObjectsTable = () => {
    return (
        <Box sx={{ height: 500, width: '100%' }}>
            <DataGrid
                localeText={ruRU.components.MuiDataGrid.defaultProps.localeText}
                rows={rows}
                columns={columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
                checkboxSelection
                disableSelectionOnClick
                // localeText={{
                //
                // }}
            />
        </Box>
    );
}