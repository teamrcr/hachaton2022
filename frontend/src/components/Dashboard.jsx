import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import FolderCopyIcon from '@mui/icons-material/FolderCopy';
import {CheckboxSelectionGrid, ObjectsTable} from "./ObjectsTable";
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';


export const TemporaryDrawer = () => {
    const list = (anchor) => (
            <List>
                {['Реестр обьектов', 'Справочники'].map((text, index) => (
                    <ListItem key={text} disablePadding>
                        <ListItemButton>
                            <ListItemIcon>
                                <FolderCopyIcon />
                            </ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
    );

    return (
        <Box sx={{display: "flex"}}>
            {/*<Drawer variant="permanent" open={true}>*/}
            {/*    {list("left")}*/}
            {/*</Drawer>*/}
            {/*<Box component="main" sx={{*/}
            {/*    flexGrow: 1,*/}
            {/*    height: '100vh',*/}
            {/*    overflow: 'auto',*/}
            {/*}}>*/}
                    <Grid container>
                        <Grid item xs={12}>
            <ObjectsTable />
                        </Grid>
                    </Grid>
            {/*</Box>*/}
        </Box>
    )
}
