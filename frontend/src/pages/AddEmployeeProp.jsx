import React from 'react';
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Container from "@mui/material/Container";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Divider from '@mui/material/Divider';
import ClearIcon from '@mui/icons-material/Clear';
import Box from '@mui/material/Box';
import Typography from "@mui/material/Typography";
import AddIcon from '@mui/icons-material/Add';




const AddEmployeeProp = () => {
    const [department, setDepertment] = React.useState('');
    const [project, setProject] = React.useState('');
    const [role, setRole] = React.useState('');

    const handleChange = (event) => {
        setDepertment(event.target.value);
    };
    const handleChange2 = (event) => {
        setProject(event.target.value);
    };
    const handleChange3 = (event) => {
        setRole(event.target.value);
    };

    return (
        <div>
            <Container component="main" maxWidth="md">
                <Typography variant="h5" sx={{margin: "20px 0"}}>Добавление нового сотрудника</Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <Typography variant="h6">Основные данные</Typography>
                        </Grid>
                        <Grid item xs={5}>
                            <Typography variant="h6">Дополнительные данные</Typography>
                        </Grid>
                        <Grid item xs={1}>
                            <AddIcon fontSize="large" color="primary" sx={{display: "flex", alignItems: "flex-end"}} />
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                                margin="normal"
                                label="ФИО"
                                fullWidth
                            />
                            <TextField
                                margin="normal"
                                label="Номер телефона"
                                fullWidth
                            />
                            <TextField
                                margin="normal"
                                label="Дата рождения"
                                fullWidth
                            />
                            <TextField
                                margin="normal"
                                label="Табельынй №"
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <FormControl fullWidth margin="normal">
                                <InputLabel id="select-label">Отдел</InputLabel>
                                <Select
                                    labelId="select-label"
                                    value={department}
                                    label="Отдел"
                                    onChange={handleChange}
                                >
                                    <MenuItem value={1}>Отдел велосипедов</MenuItem>
                                    <MenuItem value={2}>Отдел скутеров</MenuItem>
                                    <MenuItem value={3}>Отдел вертолетов</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl fullWidth margin="normal">
                                <InputLabel id="select-label2">Проект</InputLabel>
                                <Select
                                    labelId="select-label2"
                                    value={project}
                                    label="Проект"
                                    onChange={handleChange2}
                                >
                                    <MenuItem value={1}>Звезда смерти</MenuItem>
                                    <MenuItem value={2}>Единорог-31</MenuItem>
                                    <MenuItem value={3}>Метелка</MenuItem>
                                </Select>
                            </FormControl>
                            <FormControl fullWidth margin="normal">
                                <InputLabel id="select-label3">Роль</InputLabel>
                                <Select
                                    labelId="select-label3"
                                    value={role}
                                    label="Роль"
                                    onChange={handleChange3}
                                >
                                    <MenuItem value={1}>Закупщик</MenuItem>
                                    <MenuItem value={2}>Водитель</MenuItem>
                                    <MenuItem value={3}>Кладовщик</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        {/*<Divider orientation="vertical" flexItem style={{marginRight:"-1px"}} />*/}
                        <Grid item xs={6}>
                            <Box sx={{display: "flex", alignItems: "center"}}>
                            <TextField
                                margin="normal"
                                label="Уровень образования"
                                fullWidth
                            />
                            <ClearIcon sx={{color: "red", alignItems: "right", fontSize: 28}} />
                            </Box>
                            <Box sx={{display: "flex", alignItems: "center"}}>
                            <TextField
                                margin="normal"
                                label="Дополнительное образование"
                                fullWidth
                            />
                                <ClearIcon sx={{color: "red", alignItems: "right", fontSize: 28}} />
                            </Box>
                            <Box sx={{display: "flex", alignItems: "center"}}>
                            <TextField
                                margin="normal"
                                label="Владение иностранным языком"
                                fullWidth
                            />
                                <ClearIcon sx={{color: "red", alignItems: "right", fontSize: 28}} />
                            </Box>
                            <Box sx={{display: "flex", alignItems: "center"}}>
                                <TextField
                                    margin="normal"
                                    label="Нарушения"
                                    fullWidth
                                />
                                <ClearIcon sx={{color: "red", alignItems: "right", fontSize: 28}} />
                            </Box>
                            <Box sx={{display: "flex", alignItems: "center"}}>
                                <TextField
                                    margin="normal"
                                    label="Блаодарности"
                                    fullWidth
                                />
                                <ClearIcon sx={{color: "red", alignItems: "right", fontSize: 28}} />
                            </Box>
                        </Grid>
                    </Grid>
            </Container>
        </div>
    );
};

export default AddEmployeeProp;