import React from 'react';
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import {TemporaryDrawer} from "../components/Dashboard";
import { Link } from "react-router-dom";

const MainPage = () => {
    return (
        <div>
            <Box sx={{margin: "10px"}}>
                <Button component={Link} to="/add-employee" variant="outlined" sx={{marginRight: "15px"}}>Добавить</Button>
                <Button component={Link} to="/compare" variant="outlined">Сравнить</Button>
            </Box>
            <TemporaryDrawer />
        </div>
    );
};

export default MainPage;