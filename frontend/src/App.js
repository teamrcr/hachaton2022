import * as React from 'react';
import MainAppBar from "./components/AppBar";
import {
    Routes,
    Route, BrowserRouter,
} from "react-router-dom";
import './App.css';
import '@fontsource/roboto/400.css';
import MainPage from "./pages/MainPage";
import ComparePage from "./pages/ComparePage";
import AddEmployeeProp from "./pages/AddEmployeeProp";
import Dictionary from "./pages/Dictionary";


function App() {
  return (
      <BrowserRouter>
          <div className="App">
               <MainAppBar />
               <Routes>
                   <Route path="/" element={<MainPage />} exact />
                   <Route path="/add-employee" element={<AddEmployeeProp />} />
                   <Route path="/compare" element={<ComparePage />} />
                   <Route path="/dictionary" element={<Dictionary />} />
                   <Route
                       path="*"
                       element={
                           <main style={{ padding: "1rem" }}>
                               <p>Ничего не найдено!</p>
                           </main>
                       }
                   />
               </Routes>
           </div>
      </BrowserRouter>
  );
}

export default App;
